<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
*/

use Illuminate\Routing\Router;

Route::get('/', 'HomeController@index');

$router->group(['prefix' => 'product'], function (Router $router){
    $router->get('/', 'ProductController@index');

    $router->group(['prefic' => '{id}'], function(Router $router){
        $router->get('/{id}', 'ProductController@view')->name('product_view');
    });
});


Auth::routes();

