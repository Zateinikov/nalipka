<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Entity\Product;

class ProductController extends Controller
{
    /**
     * Show all products
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $products = Product::paginate(20);

        return view('product/all', compact('products'));
    }

    /**
     * Product page
     * @param integer $id
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function view($id, Request $request)
    {
        $product = Product::find($id);

        return view('product/view', compact('product'));
    }
}
