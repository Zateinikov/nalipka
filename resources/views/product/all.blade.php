@extends('home')

@section('page')
    <div class="page-header">
        <h1>Products list</h1>
        <p class="lead">All products</p>
    </div>

    <div class="row">

        @foreach ($products as $key => $product)
            <div class="col-3 col-sm-3 col-lg-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h4><a href="{{ route('product_view',['id' => $product->id]) }}">{{{ $product->name }}}</a></h4>
                        <p>Price: {{ $product->price }}</p>

                            <a class="btn btn-info btn-sm btn-add" href="#">Add Cart</a>

                            {{-- Add to wishlist button --}}
                            <span class="pull-right">
                                <a class="btn btn-xs tip wishlist-add" href="#" title="Add to Wishlist"><i class="fa fa-star-o fa-lg"></i></a>
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            @if(($key+1)%4 == 0)
                </div>
                <div class="row">
            @endif
        @endforeach

    </div>

    {!! $products->render() !!}


@endsection
