@extends('home')

@section('page')
    <div class="row">
        <div class="col-lg-12">
        <h3>Product view page</h3>
        <div class="col-lg-3">
            <img src="/img/product_default.jpg" class="img-rounded"/>
        </div>
        <div class="col-lg-9">
            <h4>{{ $product->name }}</h4>
            <p>Price: {{ $product->price }}</p>
            <p>Short description: {{ $product->short_descr }}</p>

            <p>
                <a class="btn btn-info btn-sm btn-add" href="#">Add Cart</a>
                {{-- Add to wishlist button --}}
                <span class="pull-right">
                    <a class="btn btn-xs tip wishlist-add" href="#" title="Add to Wishlist"><i class="fa fa-star-o fa-lg"></i></a>
                </span>
            </p>
        </div>
    </div>
@endsection
