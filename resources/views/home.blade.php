@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>
                    <p>
                        <ul>
                            <li>Для seed-a данных использовался генератор
                                <a href="https://github.com/fzaninotto/Faker">Faker</a></li>
                            <li> <a href="http://getbootstrap.com/">Bootstrap v3</a> - для отображения информации</li>
                        </ul>

                    </p>
                <div class="panel-body">
                    @yield('page')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
