<?php

use Illuminate\Database\Seeder;

use App\Entity\Product;
use App\Entity\Category;

class ProductsSeeder extends DatabaseSeeder {

    /**
     * Seed data for Products table
     */
    public function run(){
        DB::table('products')->delete();

        $faker = $this->getFaker();
        $categories = Category::all();
        foreach($categories as $category) {
            for($i = 0; $i <= 5; $i++) {
                Product::create(array(
                    'code' => uniqid(),
                    'name' => ucwords(implode(" ", $faker->words(2))),
                    'short_descr' => $faker->paragraph(),
                    'price' => $faker->numberBetween(2,5,100),
                    'image' => '',
                    'category_id' => $category->id,
                ));

            }

        }
    }
}