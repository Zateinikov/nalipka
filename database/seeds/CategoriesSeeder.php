<?php

use Illuminate\Database\Seeder;

use App\Entity\Category;

class CategoriesSeeder extends DatabaseSeeder {

    /**
     * Seed data for Categories table
     */
    public function run(){
        DB::table('categories')->delete();

        $faker = $this->getFaker();
        
        for ($i = 0; $i < 10; $i++) {
            Category::create(array(
                'name' => $faker->ColorName(),
            ));
        }
    }
}