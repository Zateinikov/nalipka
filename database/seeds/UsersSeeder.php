<?php

use Illuminate\Database\Seeder;

use App\User;

class UsersSeeder extends DatabaseSeeder {

    /**
     * Seed data for Users table
     */
    public function run(){
        DB::table('users')->delete();

        $faker = $this->getFaker();
        for ($i = 0; $i < 5; $i++) {
            User::create(array(
                'email' => $faker->email,
                'name' => $faker->name,
                'password'=> Hash::make('password')
            ));
        }
    }
}