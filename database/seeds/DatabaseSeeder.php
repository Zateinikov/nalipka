<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    protected $faker;

    public function getFaker()
    {
        if(empty($this->faker)) {
            $faker = \Faker\Factory::create();
            $faker->addProvider(new Faker\Provider\Base($faker));
            $faker->addProvider(new Faker\Provider\Lorem($faker));
            $faker->addProvider(new Faker\Provider\uk_UA\Person($faker));
            $faker->addProvider(new Faker\Provider\uk_UA\Color($faker));
        }
        return $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call('UsersSeeder');
        $this->command->info('Users table seeded!');
        $this->call('CategoriesSeeder');
        $this->command->info('Category table seeded!');
        $this->call('ProductsSeeder');
        $this->command->info('Product table seeded!');
        
    }
}
