# E-commerce
Application is builded on Laravel 5.3 without third-party e-commerce bundles.
I'm trying to implement simply business processes with clear and understandable architecture and logic.

## Setting up
run
```
composer install|update
```

Create and configure your .env file.  Use .env.example file as template

To create only tables without data:
```
php artisan migrate:refresh
```

Or if you want to create the schema and load data for a functional demo you can run:

```
php artisan migrate:refresh --seed
```

### Model
* Product
* Order
* Category
* Tag
* User

### To do list
* Product process (checkout)
* Customer side (his order)
* Admin side (all orders, edit products, import/export products)

## Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).
